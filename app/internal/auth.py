#local imports

#libs imports
from fastapi import APIRouter, Depends, HTTPException, status
from routers.users import users
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
#system imports
import hashlib

router = APIRouter()

def hash_password(password: str):
    return hashlib.sha256(f'{password}'.encode('utf-8')).hexdigest()

@router.post("/login")
async def login(form_data: OAuth2PasswordRequestForm = Depends()):
    for user in users:
        if user["name"] == form_data.username:
            if user["password_hash"] == hash_password(form_data.password):
                    print(users.name)
                    return {"access_token": users.name, "token_type": "bearer"}
        raise HTTPException(status_code=400, detail="Incorrect username or password")

