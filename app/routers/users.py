#Local imports
from data.datasUser import users
from models.user import User
#System imports
#Libs imports
from fastapi import APIRouter, status, HTTPException
import bcrypt

router = APIRouter()

@router.get("/users", responses={status.HTTP_204_NO_CONTENT: {}})
async def get_all_users() -> list[User]:
    if len(users) == 0:
        return Response(status_code=status.HTTP_204_NO_CONTENT)
    return users
    


@router.post("/users", status_code=status.HTTP_201_CREATED, )
async def create_user(user: User):
    passwordToHash = user.password_hash
    # Adding the salt to password
    salt = bcrypt.gensalt()
    passwordToHash = passwordToHash.encode('utf-8')
    # Hashing the password
    hashed = bcrypt.hashpw(passwordToHash, salt)
    user.password_hash = hashed
    users.append(user)
    return user



