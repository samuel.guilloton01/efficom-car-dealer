#Local imports
from data.dataCar import cars
from models.car import Car
#System imports
#Libs imports
from fastapi import APIRouter, status
import bcrypt

router = APIRouter()

@router.get("/cars", responses={status.HTTP_204_NO_CONTENT: {}})
async def get_all_cars() -> list[Car]:
    return cars

