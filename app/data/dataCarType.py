CarTypes = [
    {"id": 1, "marque": "Citroën", "model": "C3", "nb_door": "4", "nb_seat": "4"},
    {"id": 2, "marque": "Citroën", "model": "C4", "nb_door": "4", "nb_seat": "4"},
    {"id": 3, "marque": "Toyota", "model": "Yaris", "nb_door": "4", "nb_seat": "4"},
    {"id": 4, "marque": "Renault", "model": "Megane", "nb_door": "4", "nb_seat": "4"},
]