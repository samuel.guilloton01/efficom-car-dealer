users = [
    {"id": 1, "name": "Sam", "email": "sam@gmail.com", "password_hash": "f2d81a260dea8a100dd517984e53c56a7523d96942a834b9cdc249bd4e8c7aa9", "password": "azerty", "phone": "0612435465", "newsletter": "true", "is_customer": "yes"},
    {"id": 2, "name": "Val", "email": "val@gmail.com", "password_hash": "f2d81a260dea8a100dd517984e53c56a7523d96942a834b9cdc249bd4e8c7aa9", "password": "azerty", "phone": "0623543446", "newsletter": "false", "is_customer": "no"},
    {"id": 3, "name": "Lou", "email": "lou@gmail.com", "password_hash": "f2d81a260dea8a100dd517984e53c56a7523d96942a834b9cdc249bd4e8c7aa9", "password": "azerty","phone": "0623344331", "newsletter": "false", "is_customer": "no"},
    {"id": 4, "name": "Xavier", "email": "xavier@gmail.com", "password_hash": "f2d81a260dea8a100dd517984e53c56a7523d96942a834b9cdc249bd4e8c7aa9", "password": "azerty", "phone": "0634252312", "newsletter": "true", "is_customer": "yes"},
    {"id": 5, "name": "perrine", "email": "perrine@gmail.com", "password_hash": "f2d81a260dea8a100dd517984e53c56a7523d96942a834b9cdc249bd4e8c7aa9", "password": "azerty","phone": "0532546534", "newsletter": "true", "is_customer": "yes"},
]