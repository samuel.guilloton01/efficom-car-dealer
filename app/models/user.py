#system import
from enum import Enum
from datetime import datetime
from pydantic import BaseModel


class User(BaseModel):
    id: int
    name: str
    email: str
    password_hash: str
    phone: int
    newsletter: bool
    is_customer: bool
