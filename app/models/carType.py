#system import
from enum import Enum
from datetime import datetime
from pydantic import BaseModel

class CarType(BaseModel):
    id: int
    marque: str
    model: str
    nb_door: str
    nb_seat: int