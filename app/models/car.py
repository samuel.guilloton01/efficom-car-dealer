#system import
from enum import Enum
from datetime import datetime
from pydantic import BaseModel

class ColorEnum(Enum):
    RED = "RED"
    GREEN = "GREEN"
    BLUE = "BLUE"
    PINK = "PINK"
    BLACK = "BLACK"
    GREY = "GREY"

class StateEnum(Enum):
    WRECK = "WRECK"
    FAIR = "FAIR"
    GOOD_CONDITION = "GOOD CONDITION"
    NICKEL = "NICKEL"
    UNUSED = "UNUSED"
 
class Car(BaseModel):
    id: int
    nb_kilometer: int
    color: ColorEnum
    sale_price: float
    buying_price: float
    options: str
    states: StateEnum
    construction_year: datetime
    parking_place: str
    arrival_date: datetime
    expected_arrival_date: datetime
    sales_employee: int | None = None
    previous_owner: int | None = None
    new_owner: int | None = None